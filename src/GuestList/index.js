import React from 'react'
import { Link } from 'react-router-dom'
import {Row, Col, Card, CardImg, CardBody, CardTitle, CardSubtitle, CardText, Button, Container} from 'reactstrap'
import axios from 'axios'

class GuestList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            guestListArray:[]
        }
    }

    componentDidMount(){
        axios.get('https://reduxblog.herokuapp.com/api/posts?key=tugas-pri-1-portofolio')
        .then((response)=>{
            
            this.setState(state=>({
                        guestListArray: [...state.guestListArray, ...response.data]
                    }))

        })
        // .then(response=>response.json())
        // .then(data=>{
        //     this.setState(state=>({
        //         guestListArray: [...state.guestListArray, ...data]
        //     }))
        // })
    }

    render() {
        //get()
        return (
            <div style={{ textAlign: 'center' }}>

<Container style={{marginTop:"50px"}}>
            <Row className="box-row">
              <Col className="box-1 box-col-1">
                <h1 className="text-hello"> GuestList</h1>
              </Col>
              
              <Col className="box-1 box-col-2">
                <h1 className="text-friends"> Website</h1>
              </Col>
            </Row>
          </Container>
               
                

                <ul style={{listStyle:'none', justifyContent:"center", padding:0}}>
                    {
                        this.state.guestListArray.map(guestList=>{
                            return(
                                <li key={guestList.id} style={{ padding:'1em', display:"inline-block"}}>
                                    <Card style={{width:"250px"}}><Link to={`/GuestList/${guestList.id}`}>
        <CardImg top style={{width:"200px"}} src={guestList.content} alt="Card image cap" />
        <CardBody>
          <CardTitle>{guestList.title}</CardTitle>
  
          <CardText>{guestList.categories}</CardText>
          <Button>Find Out</Button>
        </CardBody>
        </Link>
      </Card>
                            
                                </li>
                            )
                        })
                    }
                </ul>
                
            </div>
                )
            }
}

export default GuestList;