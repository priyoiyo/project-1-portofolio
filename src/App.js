import React from 'react';
import logo from './logo.svg';
import './App.css';
import './Home/home.css'
import './GuestForm/GuestForm.css'
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Common/Header';
import {Home} from './Home'
import GuestForm from './GuestForm/GuestForm'
import Portofolio from './Portofolio'
import About from './About'
import GuestList from './GuestList';





function App() {
  return (
    <div className="App">
      
      <div >
      <Router>
        
      <Route exact path="/" component={GuestForm}></Route>
      
      
      <div className="flip-right">
      <Route exact path="/Home" component={Header}></Route>
      <Route path="/Home" component={Home}></Route>
      </div>
      
      <Route path="/Portofolio" component={Header}></Route>
      <Route path="/Portofolio" component={Portofolio}></Route>
      <Route path="/About" component={Header}></Route>
      <Route path="/About" component={About}></Route>
      <Route exact path="/GuestList" component={Header}></Route>
      <Route path="/GuestList" component={GuestList}></Route>
      </Router>
      
      </div>
    </div>
  );
}

export default App;
