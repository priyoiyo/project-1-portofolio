import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink} from 'reactstrap';



class Header extends React.Component{
    constructor(props){
        super(props);
        this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
}

toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });}

render() {
    return(
        <div>
            <Navbar className="header-color" expand="md">
          <NavbarBrand className="link-white" href="/Home">
            <img className="logoku" src="https://www.eslgaming.com/sites/all/themes/stability/stability_sub/logo.png"/>
            Pri-Portofolio</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
            <NavItem>
                <NavLink className="link-white" href="/Home">Home</NavLink>
              </NavItem>
            
              <NavItem>
                <NavLink className="link-white" href="/Portofolio">Portofolio</NavLink>
              </NavItem>
              
              <NavItem>
                <NavLink className="link-white" href="/About">About</NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="link-white" href="/">GuestForm</NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="link-white" href="/GuestList">GuestList</NavLink>
              </NavItem>
              
            </Nav>
          </Collapse>
        </Navbar>
        </div>
    );

}
}

export default Header;